from abc import ABC, abstractclassmethod
class Animal(ABC):

    @abstractclassmethod

    def eat(self, food):
        pass

    def make_sound(self):
        pass

    
    

class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    # Getter
    def get_name(self):
        print(f"Name of dog: {self._name}")
    
    def get_breed(self):
        print(f"Breed of dog: {self._breed}")
    
    def get_age(self):
        print(f"Age of dog: {self._age}")

    # Setter
    def set_name(self, name):
        self._name = name
    
    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    # implementing the abstract methods from the parent class
    def eat(self, food):
        print(f"Eaten {food}.")
    
    def call(self):
        print(f"Here {self._name}")
    
    def make_sound(self):
        print("Woof! Woof!")

    

        
class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    # Getter
    def get_name(self):
        print(f"Name of cat: {self._name}")
    
    def get_breed(self):
        print(f"Breed of cat: {self._breed}")
    
    def get_age(self):
        print(f"Age of cat: {self._age}")
    
    # Setter
    def set_name(self, name):
        self._name = name
    
    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    # implementing the abstract methods from the parent class
    def eat(self, food):
        print(f"Serve me {food}.")

    def call(self):
        print(f"{self._name}, come on!")
    
    def make_sound(self):
        print("Lugalugalugaluga.")


# Test Cases:
dog1 = Dog("Taliban", "Chihuahua", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Cheetos", "Munchkin", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()

