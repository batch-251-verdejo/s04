from abc import ABC, abstractclassmethod
class Animal(ABC):

    @abstractclassmethod

    def eat(self, food):
        pass

    def make_sound(self):
        pass

class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    # Getter
    def get_name(self):
        print(f"Name of dog: {self._name}")
    
    def get_breed(self):
        print(f"Breed of dog: {self._breed}")
    
    def get_age(self):
        print(f"Age of dog: {self._age}")

    # Setter
    def set_name(self, name):
        self._name = name
    
    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age
    
    def eat(self, food):
        print(f"Eaten {food}.")
    
    def call(self):
        print(f"Here {self._name}")
    
    def make_sound(self):
        print("Woof! Woof!")

    
class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    # Getter
    def get_name(self):
        print(f"Name of cat: {self._name}")
    
    def get_breed(self):
        print(f"Breed of cat: {self._breed}")
    
    def get_age(self):
        print(f"Age of cat: {self._age}")
    
    # Setter
    def set_name(self, name):
        self._name = name
    
    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    def eat(self, food):
        print(f"Serve me {food}.")

    def call(self):
        print(f"{self._name}, come on!")
    
    def make_sound(self):
        print("Lugalugalugaluga.")

dog1 = Dog("Taliban", "Chihuahua", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Cheetos", "Munchkin", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()







# Test to try if a class can affect other classes properties. Please disregard
class Fighter():
    def __init__(self, name, level, profession):
        self.name = name
        self.level = level
        self.profession = profession
    
    # Other properties
        self.attack = level * 0.5
        self.health = level * 2
    
    # Getter
    def get_name(self):
        print(f"My name is {self.name}!")

    def get_level(self):
        print(f"Character level is {self.level}")

    def get_profession(self):
        print(f"{self.name} profession is {self.profession}.")

    def get_attack(self):
        print(f"{self.name} attack power is {self.attack}")
    
    def get_health(self):
        print(f"{self.name} health is {self.health}")

    # Setter
    def set_name(self, name):
        self.name = name
        print(f"Due to infamy {self.get_name()} change his/her name to {self.name}")
    
    def set_level(self, level):
        self.level = level
    
    def set_profession(self, profession):
        self.profession = profession
        print(f"Due to financial difficulties {self.name} shift his/her career to {profession}")
    
    def set_attack(self, attack):
        self.attack = attack

    def set_health(self, health):
        self.health = health
    
    # Abstraction
    def tackle(self, opponent):
        tacklePower = self.attack * 3
        opponent.health -= tacklePower
        print(f"{self.name} tackled {opponent.name}")
        print(f"{opponent.name} take damage of {tacklePower}")

        if (opponent.health <= 0):
            opponent.faint()
    
    def punch(self, opponent):
        punchPower = self.attack
        opponent.health -= punchPower
        print(f"{opponent.name} took a straight punch to the face! Health is now {opponent.health}")
        print(f"{opponent.name} take damage of {punchPower}")

        if (opponent.health <= 0):
            opponent.faint()

    def roundHouse(self, opponent):
        kickPower = self.attack * 2
        opponent.health -= kickPower
        print(f"{opponent.name} catch {self.name}'s foot with his/her face!")
        print(f"{opponent.name} take damage of {kickPower}")

        if (opponent.health <= 0):
            opponent.faint()

    def restore_health(self, potion):
        self.health += potion
        print(f"{self.name} restored {potion} of his health")
    
    def faint(self):
        print(f"{self.name} passed out!")
    
    

fighter1 = Fighter("Chuck Norris", 99, "Actor")
fighter2 = Fighter("Wimp Loh", 2, "Kung pow character")
        
        


    
